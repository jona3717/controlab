package vistas;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import modelo.Actividades;
import modelo.ActividadesDAO;
import modelo.Sesiones;
import modelo.SesionesDAO;

/**
 *
 * @author jonathan
 */
public class ControlabCliente extends javax.swing.JFrame {

    private Actividades actividad;
    private int actSesion;

    DefaultListModel lma = new DefaultListModel();

    /**
     * Creates new form Sesion
     */
    public ControlabCliente() {

        initComponents();

        this.sesionActual();
        this.lblIdSesion.setText(this.actSesion + "");
        this.mostrarActividades();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        lstActividades = new javax.swing.JList<>();
        lblBienvenido = new javax.swing.JLabel();
        lblNombreUsuario = new javax.swing.JLabel();
        btnCerrarSesion = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        lblIdSesion = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
        });
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBackground(new java.awt.Color(249, 249, 249));
        jPanel1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lstActividades.setModel(this.lma);
        jScrollPane1.setViewportView(lstActividades);

        jPanel1.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 60, 430, 140));

        lblBienvenido.setText("Bienvenido");
        jPanel1.add(lblBienvenido, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, -1, 20));
        jPanel1.add(lblNombreUsuario, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 20, 180, 20));

        btnCerrarSesion.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/Black_50px/icons8-cierre-de-sesión-redondeado-hacia-la-izquierda-filled-20.png"))); // NOI18N
        btnCerrarSesion.setBorder(null);
        btnCerrarSesion.setBorderPainted(false);
        btnCerrarSesion.setContentAreaFilled(false);
        btnCerrarSesion.setSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/Gray_50px/icons8-cierre-de-sesión-redondeado-hacia-la-izquierda-filled-20.png"))); // NOI18N
        btnCerrarSesion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCerrarSesionActionPerformed(evt);
            }
        });
        jPanel1.add(btnCerrarSesion, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 10, 30, 30));

        jLabel1.setText("Sesión:");
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 20, -1, 20));
        jPanel1.add(lblIdSesion, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 20, 40, 20));

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 430, 200));

        pack();
    }// </editor-fold>//GEN-END:initComponents
    
    private void mostrarActividades() {
        List<String> procesos = this.listarActividades();
        this.lma.removeAllElements();
        this.lma.clear();

        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        String fecha = dateFormat.format(date);

        Date time = new Date();
        DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
        String hora = timeFormat.format(time.getTime());

        this.actividad = new Actividades();

        for (String proceso : procesos) {
            this.lma.addElement(proceso);
            this.actividad.setActividad(proceso);
            this.actividad.setDia(fecha);
            this.actividad.setHora(hora);
            this.actividad.setIdSesion(this.actSesion);
            try {
                ActividadesDAO metodo = new ActividadesDAO();
                metodo.registrarActividades(actividad);
            } catch (Exception e) {
                JOptionPane.showMessageDialog(this, e.getMessage());
            }
        }
    }

    private List listarActividades() {
        List<String> procesos = new ArrayList();
        try {
            String nomProc;
            Process proceso = Runtime.getRuntime().exec("ps -U 1000 -u jonathan");
            try (BufferedReader input = new BufferedReader(new InputStreamReader(proceso.getInputStream()))) {
                while ((nomProc = input.readLine()) != null) {
                    if (!nomProc.trim().equals("")) {
                        procesos.add(nomProc.substring(24));
                    }
                }
            }
        } catch (IOException e) {
            JOptionPane.showMessageDialog(this, e.getMessage());
        }
        return procesos;
    }

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
        Login form = new Login();
        form.setVisible(true);
    }//GEN-LAST:event_formWindowClosed

    private void btnCerrarSesionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCerrarSesionActionPerformed
        this.mostrarActividades();
        this.desactivarSesion();
        this.dispose();
    }//GEN-LAST:event_btnCerrarSesionActionPerformed

    private void sesionActual() {
        SesionesDAO metodo = new SesionesDAO();
        List<Sesiones> sesiones;

        try {
            sesiones = metodo.listarSesiones();
            int tamano = sesiones.size();
            if (tamano != 0) {
                this.actSesion = sesiones.get(tamano - 1).getCodigoSesiones();
            } else {
                this.actSesion = 1;
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, e.getMessage());
        }
    }

    private void desactivarSesion() {
        SesionesDAO metodo = new SesionesDAO();
        Sesiones nuevaSesion = new Sesiones();

        Date time = new Date();
        DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
        String hora = timeFormat.format(time.getTime());

        try {
            List<Sesiones> sesiones = metodo.listarSesiones();
            for (Sesiones sesion : sesiones) {
                if (sesion.getCodigoSesiones() == this.actSesion) {

                    nuevaSesion.setActivo(0);
                    nuevaSesion.setCodigoPC(sesion.getCodigoPC());
                    nuevaSesion.setCodigoSesiones(sesion.getCodigoSesiones());
                    nuevaSesion.setCodigoUsuario(sesion.getCodigoUsuario());
                    nuevaSesion.setFecha(sesion.getFecha());
                    nuevaSesion.setHoraInicio(sesion.getHoraInicio());
                    nuevaSesion.setHoraFin(hora);

                    metodo.modificarSesion(nuevaSesion);
                    break;
                }
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, e.getMessage());
        }

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ControlabCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ControlabCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ControlabCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ControlabCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ControlabCliente().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCerrarSesion;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblBienvenido;
    public javax.swing.JLabel lblIdSesion;
    public javax.swing.JLabel lblNombreUsuario;
    private javax.swing.JList<String> lstActividades;
    // End of variables declaration//GEN-END:variables
}
