package modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jonathan
 */
public class AmbientesDAO {

    Conexion metodo = new Conexion();
    Connection cn;

    public AmbientesDAO() throws Exception {
    }

    public void registrarAmbiente(Ambientes ambiente) throws Exception {
        PreparedStatement ps = null;
        this.cn = metodo.conectar();

        try {
            if (cn != null) {
                ps = cn.prepareCall("{call usp_RegistrarA(?,?,?)}");
                ps.setString(1, ambiente.getAmbiente());
                ps.setString(2, ambiente.getTipo());
                ps.setInt(3, 1);

                ps.executeUpdate();
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (ps != null && ps.isClosed() == false) {
                ps.close();
            }
            ps = null;
            if (cn != null && cn.isClosed() == false) {
                cn.close();
            }
            cn = null;
        }
    }

    public List<Ambientes> listarAulas() throws Exception {
        List<Ambientes> ambientes = null;
        Ambientes amb = null;
        this.cn = metodo.conectar();
        Statement st = null;
        ResultSet rs = null;
        String sql = "SELECT NomAmbiente, Tipo, Disponibilidad"
                + " FROM Ambientes WHERE Tipo LIKE 'A' ORDER BY NomAmbiente ASC";

        try {
            st = cn.createStatement();
            rs = st.executeQuery(sql);
            ambientes = new ArrayList<>();
            while (rs.next() == true) {
                amb = new Ambientes();
                amb.setAmbiente(rs.getString("NomAmbiente"));
                amb.setTipo(rs.getString("Tipo"));
                amb.setDisponibilidad(rs.getInt("Disponibilidad"));

                ambientes.add(amb);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (rs != null && rs.isClosed() == false) {
                rs.close();
            }
            rs = null;
            if (st != null && st.isClosed() == false) {
                st.close();
            }
            st = null;
            if (cn != null && cn.isClosed() == false) {
                cn.close();
            }
            cn = null;
        }

        return ambientes;
    }

    public List<Ambientes> listarLaboratorios() throws Exception {
        List<Ambientes> ambientes = null;
        this.cn = metodo.conectar();
        Ambientes amb = null;
        Statement st = null;
        ResultSet rs = null;
        String sql = "SELECT NomAmbiente, Tipo, Disponibilidad"
                + " FROM Ambientes WHERE Tipo LIKE 'L' ORDER BY NomAmbiente ASC";

        try {
            st = cn.createStatement();
            rs = st.executeQuery(sql);
            ambientes = new ArrayList<>();
            while (rs.next() == true) {
                amb = new Ambientes();
                amb.setAmbiente(rs.getString("NomAmbiente"));
                amb.setTipo(rs.getString("Tipo"));
                amb.setDisponibilidad(rs.getInt("Disponibilidad"));

                ambientes.add(amb);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (rs != null && rs.isClosed() == false) {
                rs.close();
            }
            rs = null;
            if (st != null && st.isClosed() == false) {
                st.close();
            }
            st = null;
            if (cn != null && cn.isClosed() == false) {
                cn.close();
            }
            cn = null;
        }

        return ambientes;
    }

    public void eliminarAmbiente(Ambientes ambiente) throws Exception {
        PreparedStatement ps = null;
        this.cn = metodo.conectar();
        try {
            if (cn != null) {
                ps = cn.prepareCall("{call usp_EliminarA(?)}");
                ps.setString(1, ambiente.getAmbiente());

                ps.executeUpdate();
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (ps != null && ps.isClosed() == false) {
                ps.close();
            }
            ps = null;
            if (cn != null && cn.isClosed() == false) {
                cn.close();
            }
            cn = null;
        }
    }

    public List<Ambientes> buscarAula(String busqueda) throws Exception {
        List<Ambientes> ambientes = null;
        Ambientes ambiente = null;
        this.cn = metodo.conectar();
        Statement st = null;
        ResultSet rs = null;
        String sql = "SELECT NomAmbiente, Tipo, Disponibilidad FROM Ambientes WHERE "
                + "Tipo LIKE 'A' AND "
                + "NomAmbiente LIKE CONCAT('%', '" + busqueda + "', '%') OR "
                + "Disponibilidad LIKE CONCAT('%','" + busqueda + "', '%') "
                + "ORDER BY NomAmbiente";

        try {
            st = cn.createStatement();
            rs = st.executeQuery(sql);
            ambientes = new ArrayList<>();
            while (rs.next() == true) {
                ambiente = new Ambientes();
                ambiente.setAmbiente(rs.getString("NomAmbiente"));
                ambiente.setTipo(rs.getString("Tipo"));
                ambiente.setDisponibilidad(rs.getInt("Disponibilidad"));

                ambientes.add(ambiente);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (rs != null && rs.isClosed() == false) {
                rs.close();
            }
            rs = null;
            if (st != null && st.isClosed() == false) {
                st.close();
            }
            st = null;
            if (cn != null && cn.isClosed() == false) {
                cn.close();
            }
            cn = null;
        }

        return ambientes;
    }

    public List<Ambientes> buscarLaboratorio(String busqueda) throws Exception {
        List<Ambientes> ambientes = null;
        Ambientes ambiente = null;
        this.cn = metodo.conectar();
        Statement st = null;
        ResultSet rs = null;
        String sql = "SELECT NomAmbiente, Tipo, Disponibilidad FROM Ambientes WHERE "
                + "Tipo LIKE 'L' AND "
                + "NomAmbiente LIKE CONCAT('%', '" + busqueda + "', '%') OR "
                + "Disponibilidad LIKE CONCAT('%','" + busqueda + "', '%') "
                + "ORDER BY NomAmbiente";

        try {
            st = cn.createStatement();
            rs = st.executeQuery(sql);
            ambientes = new ArrayList<>();
            while (rs.next() == true) {
                ambiente = new Ambientes();
                ambiente.setAmbiente(rs.getString("NomAmbiente"));
                ambiente.setTipo(rs.getString("Tipo"));
                ambiente.setDisponibilidad(rs.getInt("Disponibilidad"));

                ambientes.add(ambiente);
            }
        } catch (Exception e) {
            throw e;
        }
        return ambientes;
    }

}
