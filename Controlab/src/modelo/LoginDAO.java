
package modelo;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jonathan
 */
public class LoginDAO {
    Conexion metodo = new Conexion();
    Connection cn;

    public LoginDAO() throws Exception {
        this.cn = metodo.conectar();
    }
    
    public List<Personas> login() throws Exception {
        List<Personas> usuarios = null;
        Personas per = null;
        Statement st = null;
        ResultSet rs = null;
        String sql = "SELECT Codigo, Contrasena, Nombre, Tipo FROM Usuarios";

        try {
            st = cn.createStatement();
            rs = st.executeQuery(sql);
            usuarios = new ArrayList<>();
            while (rs.next() == true) {
                per = new Personas();
                per.setCodigo(rs.getString("Codigo"));
                per.setContrasena(rs.getString("Contrasena"));
                per.setTipo(rs.getString("Tipo"));
                per.setNombre(rs.getString("Nombre"));

                usuarios.add(per);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (rs != null && rs.isClosed() == false) {
                rs.close();
            }
            rs = null;
            if (st != null && st.isClosed() == false) {
                st.close();
            }
            st = null;
            if (cn != null && cn.isClosed() == false) {
                cn.close();
            }
            cn = null;
        }

        return usuarios;
    }
    
}
