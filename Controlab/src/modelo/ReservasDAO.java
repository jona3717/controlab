package modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jonathan
 */
public class ReservasDAO {
    Conexion metodo = new Conexion();
    Connection cn;
    
    public ReservasDAO() throws Exception {
        cn = metodo.conectar();
    }
    
    public void registrarReserva(Reservas reserva) throws Exception {
        PreparedStatement ps = null;
        
        try {
            if (cn != null) {
                ps = cn.prepareCall("{call usp_RegistrarRe(?,?,?,?,?,?,?)}");
                ps.setString(1, reserva.getCodUsuario());
                ps.setString(2, reserva.getAmbiente());
                ps.setString(3, reserva.getFecha());
                ps.setString(4, reserva.getHoraI());
                ps.setString(5, reserva.getHoraF());
                ps.setInt(6, reserva.getCantUsuarios());
                ps.setInt(7, reserva.getActivo());
                
                ps.executeUpdate();
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (ps != null && ps.isClosed() == false) {
                ps.close();
            }
            ps = null;
            if (cn != null && cn.isClosed() == false) {
                cn.close();                
            }
            cn = null;
        }
    }
    
    public void modificarReserva (Reservas reserva) throws Exception {
        PreparedStatement ps = null;
        Reservas res = reserva;

        try {
            if (cn != null) {
                ps = cn.prepareCall("{call usp_ModificarRe(?,?,?,?,?,?,?,?,?)}");
                ps.setString(1, res.getCodigoReserva());
                ps.setString(2, res.getCodigoReserva());
                ps.setString(3, res.getCodUsuario());
                ps.setString(4, res.getAmbiente());
                ps.setString(5, res.getFecha());
                ps.setString(6, res.getHoraI());
                ps.setString(7, res.getHoraF());
                ps.setInt(8, res.getCantUsuarios());
                ps.setInt(9, res.getActivo());

                ps.executeUpdate();
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (ps != null && ps.isClosed() == false) {
                ps.close();
            }
            ps = null;
            if (cn != null && cn.isClosed() == false) {
                cn.close();
            }
            cn = null;
        }
    }
    
    public Reservas leerReserva (Reservas reserva) throws Exception {
        Reservas res = null;
        Statement st = null;
        ResultSet rs = null;
        String sql = "SELECT ID, CodigoU, NomA, Dia, "
                + "HoraInicio, HoraFin, Capacidad, Activo FROM Reservas "
                + "WHERE ID LIKE '" + reserva.getCodigoReserva()+ "'";

        try {
            st = cn.createStatement();
            rs = st.executeQuery(sql);
            if (rs.next() == true) {
                res = new Reservas();
                res.setCodigoReserva(rs.getString("ID"));
                res.setCodUsuario(rs.getString("CodigoU"));
                res.setAmbiente(rs.getString("NomA"));
                res.setFecha(rs.getString("Dia"));
                res.setHoraI(rs.getString("HoraInicio"));
                res.setHoraF(rs.getString("HoraFin"));
                res.setCantUsuarios(rs.getInt("Capacidad"));
                res.setActivo(rs.getInt("Activo"));
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (rs != null && rs.isClosed() == false) {
                rs.close();
            }
            rs = null;
            if (st != null && st.isClosed() == false) {
                st.close();
            }
            st = null;
            if (cn != null && cn.isClosed() == false) {
                cn.close();
            }
            cn = null;
        }
        return res;
    }
    
    public List<Reservas> listarReservas() throws Exception {
        List<Reservas> reservas = null;
        Reservas reserva = null;
        Statement st = null;
        ResultSet rs = null;
        String sql = "SELECT ID, CodigoU, NomA, Dia,"
                + " HoraInicio, HoraFin, Capacidad, Activo FROM Reservas WHERE "
                + "Activo = 1 ORDER BY Dia";

        try {
            st = cn.createStatement();
            rs = st.executeQuery(sql);
            reservas = new ArrayList<>();
            while (rs.next() == true) {
                reserva = new Reservas();
                reserva.setCodigoReserva(rs.getString("ID"));
                reserva.setCodUsuario(rs.getString("CodigoU"));
                reserva.setAmbiente(rs.getString("NomA"));
                reserva.setFecha(rs.getString("Dia"));
                reserva.setHoraI(rs.getString("HoraInicio"));
                reserva.setHoraF(rs.getString("HoraFin"));
                reserva.setCantUsuarios(rs.getInt("Capacidad"));
                reserva.setActivo(rs.getInt("Activo"));

                reservas.add(reserva);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (rs != null && rs.isClosed() == false) {
                rs.close();
            }
            rs = null;
            if (st != null && st.isClosed() == false) {
                st.close();
            }
            st = null;
            if (cn != null && cn.isClosed() == false) {
                cn.close();
            }
            cn = null;
        }

        return reservas;
    }
    
    public void eliminarReserva(Reservas reserva) throws Exception {
        PreparedStatement ps = null;
        
        try {
            if (cn != null) {
                ps = cn.prepareCall("{call usp_EliminarR(?)}");
                ps.setString(1, reserva.getCodigoReserva());
                ps.executeUpdate();
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (ps != null && ps.isClosed() == false) {
                ps.close();
            }
            ps = null;
            if (cn != null && cn.isClosed() == false) {
                cn.close();
            }
            cn = null;
        }
    }
    
    public List<Reservas> buscarReservas(String busqueda) throws Exception {
        this.cn = metodo.conectar();
        List<Reservas> reservas = null;
        Reservas res = null;
        Statement st = null;
        ResultSet rs = null;
        String sql = "SELECT Dia, NomA, HoraInicio, HoraFin FROM Reservas WHERE "
                + "Dia LIKE CONCAT('%', '" + busqueda + "', '%') OR "
                + "NomA LIKE CONCAT('%','" + busqueda + "', '%') ORDER BY Dia";

        try {
            st = cn.createStatement();
            rs = st.executeQuery(sql);
            reservas = new ArrayList<>();
            while (rs.next() == true) {
                res = new Reservas();
                res.setFecha(rs.getString("Dia"));
                res.setAmbiente(rs.getString("NomA"));
                res.setHoraI(rs.getString("HoraInicio"));
                res.setHoraF(rs.getString("HoraFin"));
                
                reservas.add(res);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (rs != null && rs.isClosed() == false) {
                rs.close();
            }
            rs = null;
            if (st != null && st.isClosed() == false) {
                st.close();
            }
            st = null;
            if (cn != null && cn.isClosed() == false) {
                cn.close();
            }
            cn = null;
        }

        return reservas;
    }
}
