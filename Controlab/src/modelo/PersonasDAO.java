package modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jonathan
 */
public class PersonasDAO {

    Conexion metodo = new Conexion();
    Connection cn;

    public PersonasDAO() {
    }

    public void registrarUsuario(Personas persona) throws Exception {
        this.cn = metodo.conectar();
        PreparedStatement ps = null;
        Personas usuario = persona;

        try {
            if (cn != null) {
                ps = cn.prepareCall("{call usp_RegistrarU(?,?,?,?,?,?,?,?,?)}");
                ps.setString(1, usuario.getCodigo());
                ps.setString(2, usuario.getNombre());
                ps.setString(3, usuario.getApellidoPaterno());
                ps.setString(4, usuario.getApellidoMaterno());
                ps.setString(5, usuario.getDni());
                ps.setString(6, usuario.getCorreo());
                ps.setString(7, usuario.getContrasena());
                ps.setString(8, usuario.getCarrera());
                ps.setString(9, usuario.getTipo());
                ps.executeUpdate();
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (ps != null && ps.isClosed() == false) {
                ps.close();
            }
            ps = null;
            if (cn != null && cn.isClosed() == false) {
                cn.close();
            }
            cn = null;
        }
    }

    public void modificarUsuario(Personas persona) throws Exception {
        this.cn = metodo.conectar();
        PreparedStatement ps = null;
        Personas usuario = persona;

        try {
            if (cn != null) {
                ps = cn.prepareCall("{call usp_ModificarU(?,?,?,?,?,?,?,?,?,?)}");
                ps.setString(1, usuario.getCodigo());
                ps.setString(2, usuario.getCodigo());
                ps.setString(3, usuario.getNombre());
                ps.setString(4, usuario.getApellidoPaterno());
                ps.setString(5, usuario.getApellidoMaterno());
                ps.setString(6, usuario.getDni());
                ps.setString(7, usuario.getCorreo());
                ps.setString(8, usuario.getContrasena());
                ps.setString(9, usuario.getCarrera());
                ps.setString(10, usuario.getTipo());

                ps.executeUpdate();
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (ps != null && ps.isClosed() == false) {
                ps.close();
            }
            ps = null;
            if (cn != null && cn.isClosed() == false) {
                cn.close();
            }
            cn = null;
        }
    }

    public Personas leerUsuarios(Personas persona) throws Exception {
        this.cn = metodo.conectar();
        Personas per = null;
        Statement st = null;
        ResultSet rs = null;
        String sql = "SELECT Codigo, Nombre, ApellidoPaterno, ApellidoMaterno,"
                + "DNI, Correo, Contrasena, Carrera, Tipo FROM Usuarios "
                + "WHERE Codigo LIKE '" + persona.getCodigo() + "'";

        try {
            st = cn.createStatement();
            rs = st.executeQuery(sql);
            if (rs.next() == true) {
                per = new Personas();
                per.setCodigo(rs.getString("Codigo"));
                per.setNombre(rs.getString("Nombre"));
                per.setApellidoPaterno(rs.getString("ApellidoPaterno"));
                per.setApellidoMaterno(rs.getString("ApellidoMaterno"));
                per.setDni(rs.getString("DNI"));
                per.setCorreo(rs.getString("Correo"));
                per.setContrasena(rs.getString("Contrasena"));
                per.setCarrera(rs.getString("Carrera"));
                per.setTipo(rs.getString("Tipo"));
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (rs != null && rs.isClosed() == false) {
                rs.close();
            }
            rs = null;
            if (st != null && st.isClosed() == false) {
                st.close();
            }
            st = null;
            if (cn != null && cn.isClosed() == false) {
                cn.close();
            }
            cn = null;
        }
        return per;
    }

    public List<Personas> listarUsuarios() throws Exception {
        this.cn = metodo.conectar();
        List<Personas> usuarios = null;
        Personas per = null;
        Statement st = null;
        ResultSet rs = null;
        String sql = "SELECT Nombre, ApellidoPaterno, ApellidoMaterno, DNI, "
                + "Codigo, Carrera, Tipo FROM Usuarios WHERE Codigo NOT LIKE 'root' "
                + "ORDER BY Nombre, ApellidoPaterno";

        try {
            st = cn.createStatement();
            rs = st.executeQuery(sql);
            usuarios = new ArrayList<>();
            while (rs.next() == true) {
                per = new Personas();
                per.setNombre(rs.getString("Nombre"));
                per.setApellidoPaterno(rs.getString("ApellidoPaterno"));
                per.setApellidoMaterno(rs.getString("ApellidoMaterno"));
                per.setDni(rs.getString("DNI"));
                per.setCodigo(rs.getString("Codigo"));
                if (rs.getString("Carrera") != null) {
                    per.setCarrera(rs.getString("Carrera"));
                }
                per.setTipo(rs.getString("Tipo"));

                usuarios.add(per);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (rs != null && rs.isClosed() == false) {
                rs.close();
            }
            rs = null;
            if (st != null && st.isClosed() == false) {
                st.close();
            }
            st = null;
            if (cn != null && cn.isClosed() == false) {
                cn.close();
            }
            cn = null;
        }

        return usuarios;
    }

    public void eliminarUsuario(Personas persona) throws Exception {
        this.cn = metodo.conectar();
        PreparedStatement ps = null;
        Personas usuario = persona;
        try {
            if (cn != null) {
                ps = cn.prepareCall("{call usp_EliminarU(?)}");
                ps.setString(1, usuario.getCodigo());

                ps.executeUpdate();
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (ps != null && ps.isClosed() == false) {
                ps.close();
            }
            ps = null;
            if (cn != null && cn.isClosed() == false) {
                cn.close();
            }
            cn = null;
        }
    }

    public List<Personas> buscarUsuario(String busqueda) throws Exception {
        this.cn = metodo.conectar();
        List<Personas> usuarios = null;
        Personas per = null;
        Statement st = null;
        ResultSet rs = null;
        String sql = "SELECT Nombre, ApellidoPaterno, ApellidoMaterno, DNI,"
                + " Codigo, Carrera, Tipo FROM Usuarios WHERE Codigo NOT LIKE 'root' AND"
                + "(Codigo LIKE CONCAT('%', '" + busqueda + "', '%') OR "
                + "Nombre LIKE CONCAT('%','" + busqueda + "', '%') OR "
                + "ApellidoPaterno LIKE CONCAT('%', '" + busqueda + "', '%') OR "
                + "DNI LIKE CONCAT('%', '" + busqueda + "', '%')) ORDER BY Nombre,"
                + " ApellidoPaterno";

        try {
            st = cn.createStatement();
            rs = st.executeQuery(sql);
            usuarios = new ArrayList<>();
            while (rs.next() == true) {
                per = new Personas();
                per.setNombre(rs.getString("Nombre"));
                per.setApellidoPaterno(rs.getString("ApellidoPaterno"));
                per.setApellidoMaterno(rs.getString("ApellidoMaterno"));
                per.setDni(rs.getString("DNI"));
                per.setCodigo(rs.getString("Codigo"));
                if (rs.getString("Carrera") != null) {
                    per.setCarrera(rs.getString("Carrera"));
                } else per.setCarrera("");
                per.setTipo(rs.getString("Tipo"));

                usuarios.add(per);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (rs != null && rs.isClosed() == false) {
                rs.close();
            }
            rs = null;
            if (st != null && st.isClosed() == false) {
                st.close();
            }
            st = null;
            if (cn != null && cn.isClosed() == false) {
                cn.close();
            }
            cn = null;
        }

        return usuarios;
    }

    public boolean validarCodigo(String cod) throws Exception {
        this.cn = metodo.conectar();
        List<Personas> usuarios;
        String codigo = "";
        boolean valor = true;
        try {
            usuarios = listarUsuarios();

            for (Personas usuario : usuarios) {
                codigo = usuario.getCodigo();
                if (codigo.equals(cod)) {
                    valor = false;
                    break;
                } else {
                    valor = true;
                }
            }
        } catch (Exception e) {
            throw e;
        }
        return valor;
    }

    public boolean validarDni(String dni) throws Exception {
        this.cn = metodo.conectar();
        List<Personas> usuarios;
        String DNI = "";
        boolean valor = true;
        try {
            usuarios = listarUsuarios();

            for (Personas usuario : usuarios) {
                DNI = usuario.getDni();
                if (DNI.equals(dni)) {
                    valor = false;
                    Conexion metodo = new Conexion();
                    Connection cn;
                    break;
                } else {
                    valor = true;
                }
            }
        } catch (Exception e) {
            throw e;
        }
        return valor;
    }
}
