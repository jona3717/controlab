
package modelo;

/**
 *
 * @author jonathan
 */
public class Ambientes {
    private String ambiente;
    private String Tipo;
    private int disponibilidad;

    public Ambientes() {
        
    }
    
    public Ambientes(String ambiente, String Tipo, int disponibilidad) {
        this.ambiente = ambiente;
        this.Tipo = Tipo;
        this.disponibilidad = disponibilidad;
    }

    public String getAmbiente() {
        return ambiente;
    }

    public void setAmbiente(String ambiente) {
        this.ambiente = ambiente;
    }

    public String getTipo() {
        return Tipo;
    }

    public void setTipo(String Tipo) {
        this.Tipo = Tipo;
    }

    public String getDisponibilidad() {
        String disp = "";
        if (disponibilidad == 1) {
            disp = "Disponible";
        } else disp = "Ocupado";
        return disp;
    }

    public void setDisponibilidad(int disponibilidad) {
        this.disponibilidad = disponibilidad;
    }
}
