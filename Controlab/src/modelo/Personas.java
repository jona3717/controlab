package modelo;

public class Personas {

    private String nombre;
    private String apellidoPaterno;
    private String apellidoMaterno;
    private String codigo;
    private String contrasena;
    private String dni;
    private String correo;
    private String carrera;
    private String tipo;

    public String getNombre() {
        return nombre;
    }

    public String getTipo() {
        return tipo;
    }

    public String getTipoU() {
        String tipoU;
        if (this.getTipo().equals("U")) {
            tipoU = "Usuario";
        } else if (this.getTipo().equals("A")) {
            tipoU = "Administrador";
        } else {
            tipoU = "Desconocido";
        }
        return tipoU;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo.toUpperCase();
    }

    public void setNombre(String nombre) {
        this.nombre = nombre.toUpperCase();
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno.toUpperCase();
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno.toUpperCase();
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo.toUpperCase();
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo.toLowerCase();
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public String getCarrera() {
        return carrera;
    }

    public int getCarreraU() {
        int carreraU = -1;
        String car = this.getCarrera();

        if (car.equals("ADM")) {
            carreraU = 1;
        } else if (car.equals("DG")) {
            carreraU = 2;
        } else if (car.equals("DSI")) {
            carreraU = 3;
        } else if (car.equals("NEG")) {
            carreraU = 4;
        } else if (car.equals("CONT")) {
            carreraU = 5;
        } else if (car.equals("ECON")) {
            carreraU = 6;
        } else if (car.equals("MECAT")) {
            carreraU = 7;
        }
        return carreraU;
    }

    public void setCarrera(String carrera) {
        this.carrera = carrera.toUpperCase();
    }

    public String getNombreCompleto() {
        String nombreCompleto;
        nombreCompleto = this.getNombre() + " " + this.getApellidoPaterno() + " "
                + this.getApellidoMaterno();
        return nombreCompleto;
    }
}
