
package modelo;

/**
 *
 * @author jonathan
 */
public class Computadoras {
    private int idPC;
    private String nombrePC;    
    private String ipPC;
    private int estado;
    private String laboratorio;

    public Computadoras() {
        
    }
    
    public Computadoras(int idPC, String nombrePC, String ipPC, int estado, String laboratorio) {
        this.idPC = idPC;
        this.nombrePC = nombrePC;
        this.ipPC = ipPC;
        this.estado = estado;
        this.laboratorio = laboratorio;
    }

    public int getIdPC() {
        return idPC;
    }

    public void setIdPC(int idPC) {
        this.idPC = idPC;
    }

    public String getNombrePC() {
        return nombrePC;
    }

    public void setNombrePC(String nombrePC) {
        this.nombrePC = nombrePC;
    }

    public String getIpPC() {
        return ipPC;
    }

    public void setIpPC(String ipPC) {
        this.ipPC = ipPC;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }
    public String getEstadoPC() {
        String valor;
        
        if (this.getEstado() == 1) {
            valor = "Operativa";
        } else valor = "Inoperativa";
        
        return valor;
    }

    public String getLaboratorio() {
        return laboratorio;
    }

    public void setLaboratorio(String laboratorio) {
        this.laboratorio = laboratorio;
    }

}
