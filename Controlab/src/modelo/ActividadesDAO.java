
package modelo;

import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jonathan
 */
public class ActividadesDAO {
    Conexion metodo = new Conexion();
    Connection cn; 
    
    public ActividadesDAO() {
    }
    
    public void registrarActividades(Actividades actividad) throws Exception {
        this.cn = this.metodo.conectar();
        PreparedStatement ps = null;
        
        try {
            if (cn != null) {
                ps = cn.prepareCall("{call usp_RegistrarAc(?,?,?,?)}");
                ps.setString(1, actividad.getDia());
                ps.setString(2, actividad.getHora());
                ps.setString(3, actividad.getActividad());
                ps.setInt(4, actividad.getIdSesion());
                
                ps.executeUpdate();
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (ps != null && ps.isClosed() == false) {
                ps.close();
            } ps = null;
            if (cn != null && cn.isClosed() == false) {
                cn.close();
            } cn = null;
        }
    }
    
    public List<Actividades> listarActividades() throws Exception {
        this.cn = this.metodo.conectar();
        List<Actividades> actividades = null;
        Actividades act = null;
        Statement st = null;
        ResultSet rs = null;
        String sql = "SELECT ID, Dia, Hora, Descripcion, IDSesion "
                + "FROM Actividades ORDER BY Dia ASC";
        
        try {
            st = cn.createStatement();
            rs = st.executeQuery(sql);
            actividades = new ArrayList<>();
            while (rs.next() == true) {
                act = new Actividades();
                act.setIdActividad(rs.getInt("ID"));
                act.setDia(rs.getString("Dia"));
                act.setHora(rs.getString("Hora"));
                act.setActividad(rs.getString("Descripcion"));
                act.setIdSesion(rs.getInt("IDSesion"));

                actividades.add(act);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (cn != null && cn.isClosed() == false) {
                cn.close();
            } cn = null;
            if (st != null && st.isClosed() == false) {
                st.close();
            } st = null;
            if (rs != null && rs.isClosed() == false) {
                rs.close();
            } rs = null;
        }
        
        return actividades;
    }
}
