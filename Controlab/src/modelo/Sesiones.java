/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author jonathan
 */
public class Sesiones {
    private int codigoSesiones;
    private String codigoUsuario;
    private int codigoPC;
    private String fecha;
    private String horaInicio;
    private String horaFin;
    private int activo;

    public Sesiones () {
        
    }
    
    public Sesiones(int codigoSesiones, String codigoUsuario, int codigoPC, String fecha, String horaInicio, String horaFin) {
        this.codigoSesiones = codigoSesiones;
        this.codigoUsuario = codigoUsuario;
        this.codigoPC = codigoPC;
        this.fecha = fecha;
        this.horaInicio = horaInicio;
        this.horaFin = horaFin;
    }

    public int getCodigoSesiones() {
        return codigoSesiones;
    }

    public void setCodigoSesiones(int codigoSesiones) {
        this.codigoSesiones = codigoSesiones;
    }

    public String getCodigoUsuario() {
        return codigoUsuario;
    }

    public void setCodigoUsuario(String codigoUsuario) {
        this.codigoUsuario = codigoUsuario;
    }

    public int getCodigoPC() {
        return codigoPC;
    }

    public void setCodigoPC(int codigoPC) {
        this.codigoPC = codigoPC;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHoraInicio() {
        return horaInicio;
    }

    public void setHoraInicio(String horaInicio) {
        this.horaInicio = horaInicio;
    }

    public String getHoraFin() {
        return horaFin;
    }

    public void setHoraFin(String horaFin) {
        this.horaFin = horaFin;
    }

    public int getActivo() {
        return activo;
    }

    public void setActivo(int activo) {
        this.activo = activo;
    }
    
}
