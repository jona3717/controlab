
package modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jonathan
 */
public class ComputadorasDAO {
    Conexion metodo = new Conexion();
    Connection cn;

    public ComputadorasDAO() throws Exception {
        this.cn = metodo.conectar();
    }
    
    public void registrarPc(Computadoras pc) throws Exception{
        PreparedStatement ps = null;
        
        try {
            if (cn != null) {
                ps = cn.prepareCall("{call usp_RegistrarPC(?,?,?,?)}");
                ps.setString(1, pc.getNombrePC());
                ps.setString(2, pc.getIpPC());
                ps.setInt(3, pc.getEstado());
                ps.setString(4, pc.getLaboratorio());
                
                ps.executeUpdate();
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (ps != null && ps.isClosed() == false) {
                ps.close();
            }
            ps = null;
            if (cn != null && cn.isClosed() == false) {
                cn.close();                
            }
            cn = null;
        }
    }
    
    public List<Computadoras> listarComputadoras(String laboratorio) throws Exception {
        List<Computadoras> computadoras = null;
        Computadoras pc = null;
        Statement st = null;
        ResultSet rs = null;
        String sql = "SELECT ID, Codigo, IP, Estado, NomA"
                + " FROM PC WHERE NomA LIKE '" + laboratorio 
                +"' ORDER BY Codigo ASC";

        try {
            st = cn.createStatement();
            rs = st.executeQuery(sql);
            computadoras = new ArrayList<>();
            while (rs.next() == true) {
                pc = new Computadoras();
                pc.setIdPC(rs.getInt("ID"));
                pc.setNombrePC(rs.getString("Codigo"));
                pc.setIpPC(rs.getString("IP"));
                pc.setEstado(rs.getInt("Estado"));
                pc.setLaboratorio(rs.getString("NomA"));

                computadoras.add(pc);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (rs != null && rs.isClosed() == false) {
                rs.close();
            }
            rs = null;
            if (st != null && st.isClosed() == false) {
                st.close();
            }
            st = null;
            if (cn != null && cn.isClosed() == false) {
                cn.close();
            }
            cn = null;
        }

        return computadoras;
    }
    
    public void eliminarPC(Computadoras pc) throws Exception {
        PreparedStatement ps = null;
        
        try {
            if (cn != null) {
                ps = cn.prepareCall("{call usp_EliminarPC(?)}");
                ps.setInt(1, pc.getIdPC());
                ps.executeUpdate();
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (ps != null && ps.isClosed() == false) {
                ps.close();
            } ps = null;
            if (cn != null && cn.isClosed() == false) {
                cn .close();
            } cn = null;
        }
    }
    
    public List<Computadoras> listarTodasPC() throws Exception {
        List<Computadoras> computadoras = null;
        Computadoras pc = null;
        Statement st = null;
        ResultSet rs = null;
        String sql = "SELECT ID, Codigo, IP, Estado, NomA"
                + " FROM PC ORDER BY Codigo ASC";

        try {
            st = cn.createStatement();
            rs = st.executeQuery(sql);
            computadoras = new ArrayList<>();
            while (rs.next() == true) {
                pc = new Computadoras();
                pc.setIdPC(rs.getInt("ID"));
                pc.setNombrePC(rs.getString("Codigo"));
                pc.setIpPC(rs.getString("IP"));
                pc.setEstado(rs.getInt("Estado"));
                pc.setLaboratorio(rs.getString("NomA"));

                computadoras.add(pc);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (rs != null && rs.isClosed() == false) {
                rs.close();
            }
            rs = null;
            if (st != null && st.isClosed() == false) {
                st.close();
            }
            st = null;
            if (cn != null && cn.isClosed() == false) {
                cn.close();
            }
            cn = null;
        }

        return computadoras;
    }
}
