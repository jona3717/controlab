
package modelo;

import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jonathan
 */
public class SesionesDAO {

    Conexion metodo = new Conexion();
    Connection cn;
    
    public SesionesDAO() {
    }
    
    public List<Sesiones> listarSesiones() throws Exception {
        this.cn = metodo.conectar();
        List<Sesiones> sesiones;
        Sesiones sesion = null;
        Statement st = null;
        ResultSet rs = null;
        String sql = "SELECT codigo, CodigoU, IDPC, Dia, HoraInicio, HoraFin, "
                + "Activo FROM Sesiones WHERE Activo = 0 ORDER BY Dia, HoraInicio ASC";
        
        try {
            st = cn.createStatement();
            rs = st.executeQuery(sql);
            sesiones = new ArrayList<>();
            while (rs.next() == true) {
                sesion = new Sesiones();
                sesion.setCodigoSesiones(rs.getInt("codigo"));
                sesion.setCodigoUsuario(rs.getString("CodigoU"));
                sesion.setCodigoPC(rs.getInt("IDPC"));
                sesion.setFecha(rs.getString("Dia"));
                sesion.setHoraInicio(rs.getString("HoraInicio"));
                sesion.setHoraFin(rs.getString("HoraFin"));
                sesion.setActivo(rs.getInt("Activo"));

                sesiones.add(sesion);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (cn != null && cn.isClosed() == false) {
                cn.close();
            } cn = null;
            if (st != null && st.isClosed() == false) {
                st.close();
            } st = null;
            if (rs != null && rs.isClosed() == false) {
                rs.close();
            } rs = null;
        }
        
        return sesiones;
    }
    
    public List<Sesiones> listarSesionesActivas() throws Exception {
        this.cn = metodo.conectar();
        List<Sesiones> sesiones;
        Sesiones sesion = null;
        Statement st = null;
        ResultSet rs = null;
        String sql = "SELECT codigo, CodigoU, IDPC, Dia, HoraInicio, HoraFin, "
                + "Activo FROM Sesiones WHERE Activo = 1 ORDER BY Dia, HoraInicio ASC";
        
        try {
            st = cn.createStatement();
            rs = st.executeQuery(sql);
            sesiones = new ArrayList<>();
            while (rs.next() == true) {
                sesion = new Sesiones();
                sesion.setCodigoSesiones(rs.getInt("codigo"));
                sesion.setCodigoUsuario(rs.getString("CodigoU"));
                sesion.setCodigoPC(rs.getInt("IDPC"));
                sesion.setFecha(rs.getString("Dia"));
                sesion.setHoraInicio(rs.getString("HoraInicio"));
                sesion.setHoraFin(rs.getString("HoraFin"));
                sesion.setActivo(rs.getInt("Activo"));

                sesiones.add(sesion);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (cn != null && cn.isClosed() == false) {
                cn.close();
            } cn = null;
            if (st != null && st.isClosed() == false) {
                st.close();
            } st = null;
            if (rs != null && rs.isClosed() == false) {
                rs.close();
            } rs = null;
        }
        
        return sesiones;
    }

    public void registrarSesion(Sesiones sesion) throws Exception {
        this.cn = metodo.conectar();
        PreparedStatement ps = null;
        
        try {
            if (cn != null) {
                ps = cn.prepareCall("{call usp_RegistrarSe(?,?,?,?,?,?)}");
                ps.setString(1, sesion.getCodigoUsuario());
                ps.setInt(2, sesion.getCodigoPC());
                ps.setString(3, sesion.getFecha());
                ps.setString(4, sesion.getHoraInicio());
                ps.setString(5, sesion.getHoraFin());
                ps.setInt(6, sesion.getActivo());
                
                ps.executeUpdate();
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (ps != null && ps.isClosed() == false) {
                ps.close();
            }
            ps = null;
            if (cn != null && cn.isClosed() == false) {
                cn.close();
            }
            cn = null;
        }
    }
    
    public void modificarSesion(Sesiones sesion) throws Exception {
        this.cn = metodo.conectar();
        PreparedStatement ps = null;

        try {
            if (cn != null) {
                ps = cn.prepareCall("{call usp_ModificarSe(?,?,?,?,?,?,?)}");
                ps.setInt(1, sesion.getCodigoSesiones());
                ps.setString(2, sesion.getCodigoUsuario());
                ps.setInt(3, sesion.getCodigoPC());
                ps.setString(4, sesion.getFecha());
                ps.setString(5, sesion.getHoraInicio());
                ps.setString(6, sesion.getHoraFin());
                ps.setInt(7, sesion.getActivo());

                ps.executeUpdate();
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (ps != null && ps.isClosed() == false) {
                ps.close();
            }
            ps = null;
            if (cn != null && cn.isClosed() == false) {
                cn.close();
            }
            cn = null;
        }
    }
    
    public List<Sesiones> buscarSesiones(String busqueda) throws Exception {
        this.cn = metodo.conectar();
        List<Sesiones> sesiones = null;
        Sesiones ses = null;
        Statement st = null;
        ResultSet rs = null;
        String sql = "SELECT Dia, HoraInicio, HoraFin, IDPC FROM Sesiones WHERE "
                + "Dia LIKE CONCAT('%', '" + busqueda + "', '%') OR "
                + "CodigoU LIKE CONCAT('%','" + busqueda + "', '%') ORDER BY Dia";

        try {
            st = cn.createStatement();
            rs = st.executeQuery(sql);
            sesiones = new ArrayList<>();
            while (rs.next() == true) {
                ses = new Sesiones();
                ses.setFecha(rs.getString("Dia"));
                ses.setHoraInicio(rs.getString("HoraInicio"));
                ses.setHoraFin(rs.getString("HoraFin"));
                ses.setCodigoPC(rs.getInt("IDPC"));
                
                sesiones.add(ses);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (rs != null && rs.isClosed() == false) {
                rs.close();
            }
            rs = null;
            if (st != null && st.isClosed() == false) {
                st.close();
            }
            st = null;
            if (cn != null && cn.isClosed() == false) {
                cn.close();
            }
            cn = null;
        }

        return sesiones;
    }
}
