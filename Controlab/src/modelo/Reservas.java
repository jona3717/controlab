package modelo;

/**
 *
 * @author jonathan
 */
public class Reservas {
    
    private String codigoReserva;
    private String codUsuario;
    private String ambiente;
    private String fecha;
    private String horaI;
    private String horaF;
    private int cantUsuarios;
    private int activo;

    public String getCodigoReserva() {
        return codigoReserva;
    }

    public void setCodigoReserva(String codigoReserva) {
        this.codigoReserva = codigoReserva;
    }

    public String getCodUsuario() {
        return codUsuario;
    }

    public void setCodUsuario(String codUsuario) {
        this.codUsuario = codUsuario;
    }

    public String getAmbiente() {
        return ambiente;
    }

    public void setAmbiente(String ambiente) {
        this.ambiente = ambiente;
    }

    public String getFecha() {
        return fecha;
    }
    
    public String mostrarFecha() {
        String date, dia, mes, anio, nuevaFecha;
        date = fecha;
        anio = date.substring(0,4);
        mes = date.substring(5,7);
        dia = date.substring(8);
        nuevaFecha = dia + "/" + mes + "/" + anio;
        
        return nuevaFecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHoraI() {
        return horaI;
    }

    public void setHoraI(String horaI) {
        this.horaI = horaI;
    }

    public String getHoraF() {
        return horaF;
    }

    public void setHoraF(String horaF) {
        this.horaF = horaF;
    }

    public int getCantUsuarios() {
        return cantUsuarios;
    }

    public void setCantUsuarios(int cantUsuarios) {
        this.cantUsuarios = cantUsuarios;
    }

    public int getActivo() {
        return activo;
    }

    public void setActivo(int activo) {
        this.activo = activo;
    }
}
