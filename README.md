# Controlab

Sistema de control de laboratorios y aulas.

Este sistema es desarrollado por alumnos del 2° ciclo de la carrera de Desarrollo de Sistemas de Información, haciendo uso del lenguaje Java y el IDE Apache Netbeans.

Este sistema, permite visualizar las tareas que realiza el usuario en los ordenadores de distintos ambientes de una institución educativa.

